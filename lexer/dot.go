package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"regexp"
)

var (
	comLine = regexp.MustCompile("^\\.\\. *")

	tickTarget   = regexp.MustCompile("^\\.\\. _`.+?`: *")
	exTickTarget = regexp.MustCompile("^\\.\\. _`(.+?)`: *")
	target       = regexp.MustCompile("^\\.\\. _\\S+?: *")
	exTarget     = regexp.MustCompile("^\\.\\. _(\\S+?): *")

	keyDomDir = regexp.MustCompile("^\\.\\. \\|\\S+?\\| \\S.*?:\\S.*?:: *")
	keyDefDir = regexp.MustCompile("^\\.\\. \\|\\S+?\\| \\S.*?:: *")
	domDir    = regexp.MustCompile("^\\.\\. \\S.*?:\\S.*?:: *")
	defDir    = regexp.MustCompile("^\\.\\. \\S.*?:: *")

	exKeyDomDir = regexp.MustCompile("^\\.\\. \\|(\\S+?)\\| (\\S.*?):(\\S.*?):: *")
	exKeyDefDir = regexp.MustCompile("^\\.\\. \\|(\\S+?)\\| (\\S.*?):: *")
	exDomDir    = regexp.MustCompile("^\\.\\. (\\S.*?):(\\S.*):: *")
	exDefDir    = regexp.MustCompile("^\\.\\. (\\S.*?):: *")

	footnote   = regexp.MustCompile("^\\.\\. \\[.+?\\] *")
	exFootnote = regexp.MustCompile("^\\.\\. \\[(.+?)\\] *")

	endnote    = regexp.MustCompile("^\\.\\. \\[\\[.+?\\]\\] *")
	exEndnote  = regexp.MustCompile("^\\.\\. \\[\\[(.+?)\\]\\] *")
)

func (l *Lexer) lexDots(tok *token.Token) {
	if tok.Secunde == '.' {
		l.lexDDots(tok)
		return
	}
	tok.Type = token.Text
}
func (l *Lexer) setDirect(tok *token.Token, key, dom, dir string, re *regexp.Regexp) {
	tok.Type = token.Directive
	tok.Key = key
	tok.Dom = dom
	tok.Dir = dir
	tok.Clear(re)

}
func (l *Lexer) exNote(tok *token.Token, t token.Type, re *regexp.Regexp, clear *regexp.Regexp) {
	res := re.FindAllStringSubmatch(tok.Text, -1)
	if len(res) > 0 {
		if len(res[0]) > 1 {
			tok.Type = t
			tok.Key = res[0][1]
			tok.Clear(clear)
			return
		}
	}
	tok.Type = token.Comment
	tok.Clear(comLine)
}

func (l *Lexer) lexDDots(tok *token.Token) {
	if tok.Terte == ' ' {

		if tok.Quarte == '_' {
			l.lexTarget(tok)
			return
		} else if tok.Quarte == '|' {

			if keyDomDir.MatchString(tok.Text) {
				res := exKeyDomDir.FindAllStringSubmatch(tok.Text, -1)
				if len(res) > 0 {
					sres := res[0]
					if len(sres) > 3 {
						l.setDirect(tok, sres[1], sres[2], sres[3], keyDomDir)
						return
					}
				}
			} else if keyDefDir.MatchString(tok.Text) {
				res := exKeyDefDir.FindAllStringSubmatch(tok.Text, -1)
				if len(res) > 0 {
					sres := res[0]
					if len(sres) > 2 {
						l.setDirect(tok, sres[1], "", sres[2], keyDefDir)
						return
					}
				}
			}
		} else if tok.Quarte == '[' {
			if tok.Quinte == '[' {
				if endnote.MatchString(tok.Text) {
					l.exNote(tok, token.Endnote, exEndnote, endnote)
					return
				}
			} else if footnote.MatchString(tok.Text) {
				l.exNote(tok, token.Footnote, exFootnote, footnote)
				return
			}
		} else if domDir.MatchString(tok.Text) {
			res := exDomDir.FindAllStringSubmatch(tok.Text, -1)
			if len(res) > 0 {
				sres := res[0]
				if len(sres) > 2 {
					l.setDirect(tok, "", sres[1], sres[2], domDir)
					tok.Key = tok.Text
					return
				}
			}
		} else if defDir.MatchString(tok.Text) {
			res := exDefDir.FindAllStringSubmatch(tok.Text, -1)
			if len(res) > 0 {
				sres := res[0]
				if len(sres) > 1 {
					l.setDirect(tok, "", "", sres[1], defDir)
					tok.Key = tok.Text
					return
				}
			}
		}
	} else if l.isHeadline(tok) {
		tok.Type = token.Headline
		return
	}

	tok.Clear(comLine)
	tok.Type = token.Comment
}

func (l *Lexer) lexTarget(tok *token.Token) {
	if tickTarget.MatchString(tok.Text) {
		tok.Type = token.Target
		l.extractTargetKey(tok, exTickTarget, tickTarget)
		return
	} else if target.MatchString(tok.Text) {
		tok.Type = token.Target
		l.extractTargetKey(tok, exTarget, target)
		return
	}
	tok.Clear(comLine)
	tok.Type = token.Comment
}

func (l *Lexer) extractTargetKey(tok *token.Token, re *regexp.Regexp, clear *regexp.Regexp) {
	res := re.FindAllStringSubmatch(tok.Text, -1)
	if len(res) > 0 {
		if len(res[0]) > 1 {
			tok.Key = res[0][1]
			tok.Clear(clear)
		}
	}
}
