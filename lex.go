package linear

import (
   "encoding/json"
   "fmt"
   "strings"
   "time"
   "gitlab.com/avocet-tools/linear/lexer"
   "gitlab.com/avocet-tools/linear/token"
)


func RunLex(args []string){
   initLogger()
   info("Called lex operation")
   start := time.Now()
   toks := Lex("stdin", strings.Join(args, "\n"))

   debug("Marshaling Tokens to JSON")
   data, err := json.MarshalIndent(toks, " ", " ")
   if err != nil {
      fatal(err)
   }
   trace("Marshal Complete")
   fmt.Println(string(data))
   fmt.Println("Lexical Analysis Complete:", len (toks), "in", time.Since(start))
}

func Lex(fname, raw string) []*token.Token {
   trace("Lexing %d char string", len(raw))
   l := lexer.New(fname, raw)
   toks := []*token.Token{}
   tok := l.Next()

   for tok.Type != token.EOF {
      toks = append(toks, tok)
      tok = l.Next()
   }
   toks = append(toks, l.Next())

   return toks
}
