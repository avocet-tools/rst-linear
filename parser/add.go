package parser

import (
   "gitlab.com/avocet-tools/ast"
)

func (p *Parser) parseSlug(){
   p.idrefs = append(p.idrefs, p.prime.Key)
}

func (p *Parser) fetchIdrefs() []string {
   idrefs := p.idrefs
   p.idrefs = []string{}
   return idrefs
}

func (p *Parser) add(blk *ast.Block){
   blk.Idrefs = p.fetchIdrefs()
   p.con.Data = append(p.con.Data, blk)
}

func (p *Parser) setMeta(){
   p.con.Meta[p.prime.Key] = p.prime.Text
}

func (p *Parser) sec(sec *ast.Section){
   sec.Idrefs = p.fetchIdrefs()
   level, ok := p.charMap[sec.LiteralLevel]
   if !ok {
      level = len(p.levelMap)
      p.charMap[sec.LiteralLevel] = level
   }
   p.levelMap[level] = sec.Content
   parentLevel := level - 1
   parent, ok := p.levelMap[parentLevel]
   if !ok {
      parent = p.baseCon
   }
   parent.Sect(sec)

}
