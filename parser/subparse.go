package parser

import (
   "strings"
   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/linear/token"
   "gitlab.com/avocet-tools/inlinear"
)


func (p *Parser) subparse(toks []*token.Token) *ast.Content {
   //ntoks := []*token.Token{}
   /*
   for _, tok := range toks {
      if tok.Type == token.IndentMeta || tok.Type == token.Meta {
         con.Meta[tok.Key] = tok.Text
         continue
      }
      ntoks = append(ntoks, tok)
   }*/
   sp := Renew(p.file, toks)
   return sp.Parse()
}

func (p *Parser) preserve(toks []*token.Token) *ast.Content {
   con := ast.NewContent()
   text := []string{}
   for _, tok := range toks {
      if tok.Type == token.IndentMeta || tok.Type == token.Meta {
         con.Meta[tok.Key] = tok.Text
      }
      text = append(text, tok.Text)
   }
   con.Internal["pre"] = strings.Join(text, "\n")

   return con
   
}

func (p *Parser) parseInline(text string, line int) []*ast.Element{
   return inlinear.Parse(p.file, text, line)
}

func (p *Parser) getText(es []*ast.Element) string {
   text := []string{}
   for _, e := range es {
      text = append(text, e.Text)
   }
   return strings.Join(text, "")
}

