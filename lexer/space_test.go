package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"testing"
)

func Test_space(t *testing.T) {
	name := "lexer.Test_space"
	data := []struct {
		raw   string
		ttype token.Type
		text  string
	}{
		{"Text", token.Text, "Text"},
		{"", token.Empty, ""},
		{"   ", token.Empty, ""},
		{"  Text", token.Indent, "  Text"},
		{"   Text", token.Indent, "   Text"},
		{"| Text", token.Block, "Text"},
	}

	for pos, datum := range data {
		l := New("Test_space.go", datum.raw)
		tok := l.Next()

		if tok.Type != datum.ttype {
			t.Fatalf(
				"%s %d: lexer returned wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(datum.ttype))
		}

		if tok.Text != datum.text {
			t.Fatalf(
				"%s %d: lexer set wrong text on token: %q != %q",
				name, pos, tok.Text, datum.text)
		}
		if tok.Line != 1 {
			t.Fatalf(
				"%s %d: lexer set wrong line number on token: %d != %d",
				name, pos, tok.Line, 1)
		}

		// Check EOF Token
		tok = l.Next()
		if tok.Type != token.EOF {
			t.Fatalf(
				"%s %d: lexer failed to return EOF: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(token.EOF))
		}
	}

}
