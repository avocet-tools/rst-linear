package parser

import (
   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/linear/preparser"
   "gitlab.com/avocet-tools/linear/token"
   "gitlab.com/avocet-tools/log"
)

// Parser provides methods to linear parse reStructuredText. 
type Parser struct {
   l *preparser.Preparser
   file string

   baseCon *ast.Content
   con *ast.Content

   prime *token.Token
   secunde *token.Token

   idrefs []string

   trace func(...any)
   debug func(...any)
   info func(...any)
   warn func(...any)
   err func(...any)
   fatal func(...any)

   tracef func(string, ...any)
   debugf func(string, ...any)
   infof func(string, ...any)
   warnf func(string, ...any)
   errf func(string, ...any)
   fatalf func(string, ...any)

   // Section Pointers
   levelMap map[int]*ast.Content
   charMap map[string]int
}

func New(fname, raw string) *Parser {
   p := &Parser{
      l: preparser.New(fname, raw),
      file: fname,
   }
   p.initialize()

   return p
}

func Renew(fname string, toks []*token.Token) *Parser {
   p := &Parser{
      l: preparser.Renew(fname, toks),
   }
   p.initialize()

   return p
}

func (p *Parser) initialize(){
   // Configure Logger
   lgr := log.New("linear", "linear:parser")
   p.trace, p.tracef = lgr.Trace()
   p.debug, p.debugf = lgr.Debug()
   p.info, p.infof = lgr.Info()
   p.warn, p.warnf = lgr.Warn()
   p.err, p.errf = lgr.Error()
   p.fatal, p.fatalf = lgr.Fatal()

   // Initialize Content
   p.baseCon = ast.NewContent()
   p.idrefs = []string{}

   // Initialize Tokens
   p.prime = p.l.Next()
   p.secunde = p.l.Next()

   // Init Heading Maps
   p.levelMap = make(map[int]*ast.Content)
   p.charMap = make(map[string]int)
   p.levelMap[0] = p.baseCon
}

func (p *Parser) read() {
   p.prime = p.secunde
   p.secunde = p.l.Next()
}

// Parse parses content from the string and returns a Content pointer for reference.
func (p *Parser) Parse() *ast.Content {
   p.con = p.baseCon

   for p.prime.Type != token.EOF {
      p.analyze()
      p.read()
   }

   return p.baseCon
}
