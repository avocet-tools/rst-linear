package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"regexp"
)

var (
	meta      = regexp.MustCompile("^:\\S.+?: *")
	exMeta    = regexp.MustCompile("^:(\\S.+?): *")
	indMeta   = regexp.MustCompile("^   :\\S.+?: *")
	exIndMeta = regexp.MustCompile("^   :(\\S.+?): *")
)

func (l *Lexer) lexColon(tok *token.Token) {
	if l.isHeadline(tok) {
		tok.Type = token.Headline
		return
	} else if meta.MatchString(tok.Text) {
		l.lexMeta(tok, token.Meta, token.Text, exMeta, meta)
		return
	}
	tok.Type = token.Text
}

func (l *Lexer) lexIndColon(tok *token.Token) {
	if indMeta.MatchString(tok.Text) {
		l.lexMeta(tok, token.IndentMeta, token.Indent, exIndMeta, indMeta)
		return
	}
	tok.Type = token.Indent
}

func (l *Lexer) lexMeta(tok *token.Token, t, f token.Type, re, clear *regexp.Regexp) {
	res := re.FindAllStringSubmatch(tok.Text, -1)
	if len(res) > 0 {
		if len(res[0]) > 1 {
			tok.Type = t
			tok.Key = res[0][1]
			tok.Clear(clear)
			return
		}
	}
	tok.Type = f
}
