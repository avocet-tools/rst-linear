package lexer

import (
	"fmt"
	"gitlab.com/avocet-tools/linear/token"
	"regexp"
	"strings"
)

func compileHead(char string) *regexp.Regexp {
	return regexp.MustCompile(fmt.Sprintf("^%s+? *$", strings.Repeat(char, 3)))
}

var headMap = map[rune]*regexp.Regexp{
	'*': compileHead("\\*"),
	'+': compileHead("\\+"),
	'-': compileHead("-"),
	'.': compileHead("\\."),
   '!': compileHead("!"),
   '"': compileHead("\""),
   '#': compileHead("#"),
   '$': compileHead("\\$"),
   '%': compileHead("%"),
   '&': compileHead("&"),
   '\'': compileHead("'"),
   '(': compileHead("\\("),
   ')': compileHead("\\)"),
   ',': compileHead(","),
   '/': compileHead("/"),
   ':': compileHead(":"),
   ';': compileHead(";"),
   '<': compileHead("<"),
   '=': compileHead("="),
   '>': compileHead(">"),
   '?': compileHead("\\?"),
   '@': compileHead("@"),
   '[': compileHead("\\["),
   '\\': compileHead("\\\\"),
   ']': compileHead("\\]"),
   '^': compileHead("\\^"),
   '_': compileHead("_"),
   '`': compileHead("`"),
   '{': compileHead("{"),
   '|': compileHead("\\|"),
   '}': compileHead("}"),
   '~': compileHead("~"),
}

func (l *Lexer) isHeadline(tok *token.Token) bool {
	if tok.Prime == tok.Secunde && tok.Secunde == tok.Terte {
		re, ok := headMap[tok.Prime]
		if ok {
			return re.MatchString(tok.Text)
		}
	}
	return false
}
