package parser

import (
   "strings"
   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/linear/token"
)


func (p *Parser) parseUList(){
   blk := ast.NewBlock(ast.UList, p.file, p.prime.Line)
   p.parseList(blk, ast.UListItem, token.UListItem)
   p.add(blk)
}

func (p *Parser) parseOList(){
   blk := ast.NewBlock(ast.OList, p.file, p.prime.Line)
   p.parseList(blk, ast.OListItem, token.OListItem)
   p.add(blk)
}

func (p *Parser) parseDList(){
   blk := ast.NewBlock(ast.DList, p.file, p.prime.Line)
   p.parseDListItem(blk, p.prime)
   for p.secunde.Type == token.DListItem {
      p.parseDListItem(blk, p.secunde)
      p.read()
   }
   p.add(blk)
}

func (p *Parser) parseDListItem(blk *ast.Block, tok *token.Token){
   li := ast.NewBlock(ast.DListItem, p.file, tok.Line)
   li.Content = p.subparse(tok.Sub)

   split := strings.Split(tok.Text, " : ")
   term := split[0]
   if len(split) > 1 {
      li.Class = split[1:]
   }
   li.Content.Inline = p.parseInline(term, li.Line)
   li.Content.Internal["key"] = p.getText(li.Content.Inline)

   blk.Content.Append(li)
}

func (p *Parser) parseList(blk *ast.Block, b ast.BType, t token.Type){
   p.parseListItem(blk, b, p.prime)
   for p.secunde.Type == t {
      p.parseListItem(blk, b, p.secunde)
      p.read()
   }
}

func (p *Parser) parseListItem(blk *ast.Block, b ast.BType, tok *token.Token){
   li := ast.NewBlock(b, p.file, tok.Line)
   li.Content = p.subparse(append([]*token.Token{tok}, tok.Sub...))
   li.Content.Internal["status"] = p.prime.Key
   blk.Content.Append(li)
}
