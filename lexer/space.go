package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"regexp"
)

var (
	emptyLine = regexp.MustCompile("^ *$")
	blockLine = regexp.MustCompile("^\\| *")
)

func (l *Lexer) lexSpace(tok *token.Token) {
	if tok.Secunde == ' ' && tok.Terte == ' ' && tok.Quarte == ':' {
		l.lexIndColon(tok)
		return
	} else if emptyLine.MatchString(tok.Text) {
		tok.Type = token.Empty
		tok.Set("")
		return
	}

	if tok.Secunde != ' ' {
		tok.Set(" " + tok.Text)
	}
	tok.Type = token.Indent
}

func (l *Lexer) lexBlock(tok *token.Token) {
	if tok.Secunde == ' ' || tok.Secunde == 0 {
		tok.Type = token.Block
		tok.Clear(blockLine)
		return
	} else if l.isHeadline(tok){
      tok.Type = token.Headline
      return
   }
	tok.Type = token.Text
}
