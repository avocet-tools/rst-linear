package lexer

import (
	"strings"

	"gitlab.com/avocet-tools/linear/token"
	"gitlab.com/avocet-tools/log"
)

// Lexer provides struct and methods for perfomring linear lexical analysis on
// reStructuredText strings.
type Lexer struct {
	in      []*token.Token
	line    *token.Token
	eof     bool
	size    int
	pos     int
	readPos int

	file string

	// Logger Functions
	trace  func(...any)
	debug  func(...any)
	info   func(...any)
	warn   func(...any)
	err    func(...any)
	fatal  func(...any)
	tracef func(string, ...any)
	debugf func(string, ...any)
	infof  func(string, ...any)
	warnf  func(string, ...any)
	errf   func(string, ...any)
	fatalf func(string, ...any)
}

// New takes a filename and raw string and returns a Lexer pointer.
func New(fname, raw string) *Lexer {
	lines := strings.Split(raw, "\n")

	l := &Lexer{ 
      file: fname,
      in: []*token.Token{},
      size: len(lines),
   }

   for pos, line := range lines {
      l.in = append(l.in, token.New(fname, line, pos+1))
   }

   l.initialize()
	return l
}

// Renew takes a filename and a token array and returns a Lexer pointer
func Renew(fname string, toks []*token.Token) *Lexer {
   l := &Lexer{
      file: fname,
      in: toks,
      size: len(toks),
   }
   l.initialize()
   return l
}

func (l *Lexer) initialize(){
	lgr := log.New("linear", "linear:lexer")
	l.trace, l.tracef = lgr.Trace()
	l.debug, l.debugf = lgr.Debug()
	l.info, l.infof = lgr.Info()
	l.warn, l.warnf = lgr.Warn()
	l.err, l.errf = lgr.Error()
	l.fatal, l.fatalf = lgr.Fatal()

	// Read First Line
	l.read()

}

func (l *Lexer) read() {
	if l.pos >= l.size {
		l.line = token.New(l.file, "", l.pos)
      l.line.Type = token.EOF
		l.eof = true
		return
	}
	l.line = l.in[l.pos]
	l.pos++
}

// Next analyzes the next line in the sequence and returns a token
func (l *Lexer) Next() *token.Token {
	tok := l.line

	l.analyze(tok)
	l.read()

	return tok
}
