package preparser

import (
   "gitlab.com/avocet-tools/log"
   "gitlab.com/avocet-tools/linear/lexer"
   "gitlab.com/avocet-tools/linear/token"

   "github.com/spf13/viper"
)

// Preparser performs intermediary analysis on the token stream, grouping like tokens to 
// prepare them for the parser.
type Preparser struct {
   l *lexer.Lexer
   file string
   pos int

   prime *token.Token
   secunde *token.Token
   terte *token.Token

   trace func(...any)
   debug func(...any)
   info func(...any)
   warn func(...any)
   err func(...any)
   fatal func(...any)

   tracef func(string, ...any)
   debugf func(string, ...any)
   infof func(string, ...any)
   warnf func(string, ...any)
   errf func(string, ...any)
   fatalf func(string, ...any)

   dom string

}

// New takes a filename and raw string and returns a Preparser pointer.
func New(fname, raw string) *Preparser {
   p := &Preparser{
      l: lexer.New(fname, raw),
      file: fname,
   }
   p.initialize()
   return p
}

// Renew takes a filename and an array of linear tokens and returns a pointer
// for a Preparser.
func Renew(fname string, toks []*token.Token) *Preparser {
   p := &Preparser{
      l: lexer.Renew(fname, toks),
      file: fname,
   }
   p.initialize()

   return p
}

func (p *Preparser) initialize(){
   lgr := log.New("linear", "linear:preparser")
   p.trace, p.tracef = lgr.Trace()
   p.debug, p.debugf = lgr.Debug()
   p.info, p.infof = lgr.Info()
   p.warn, p.warnf = lgr.Warn()
   p.err, p.errf = lgr.Error()
   p.fatal, p.fatalf = lgr.Fatal()

   p.prime = p.l.Next()
   p.secunde = p.l.Next()
   p.terte = p.l.Next()

   p.dom = viper.GetString("rst.default_domain")
   if p.dom == "" {
      p.dom = "rst"
   }
}

func (p *Preparser) read() {
   p.prime = p.secunde
   p.secunde = p.terte
   p.terte = p.l.Next()
}

// Next returns the next token in the stream, which may include addition tokens in the Sub
// array where relevant.
func (p *Preparser) Next() *token.Token {
   tok := p.prime
   // Adjust for EOF and Headings
   switch p.prime.Type {
   case token.EOF:
      return tok
   case token.Headline:
      if p.secunde.Type == token.Text && p.terte.Type == token.Headline {
         tok.Append(p.secunde)
         p.read()
         p.read()
         p.read()
         return tok
      }
   case token.Text:
      if p.secunde.Type == token.Headline {
         p.secunde.Append(tok)
         tok = p.secunde
         p.read()
         p.read()
         return tok
      } else if p.secunde.Type == token.Indent {
         tok.Type = token.DListItem
      }
   }

   p.analyze(tok)
   p.read()
   return tok
}
