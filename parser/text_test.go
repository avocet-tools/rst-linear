package parser

import (
   "strings"
   "testing"

   "gitlab.com/avocet-tools/ast"
)

func Test_text(t *testing.T){
   name := "parser.Test_text"
   data := []struct{
      raw []string
      size int
      idref string
   }{
      {[]string{"Text"}, 1, "" },
      {[]string{"Test text"}, 1, ""},
      {[]string{"Test", "text"}, 1, ""},
      {[]string{"Test", "**bold** text"}, 3, ""},
      {[]string{".. _key:", "", "Test", "text"}, 1, "key"},
   }

   for pos, datum := range data {
      p := New("text_test.go", strings.Join(datum.raw, "\n"))
      con := p.Parse()

      size := len(con.Data)
      if size != 1 {
         t.Fatalf(
            "%s %d: parser returned wrong content: %d != 1",
            name, pos, size)
      }
      blk := con.Data[0]

      if blk.Type != ast.Para {
         t.Fatalf(
            "%s %d: parser returned wrong block type: %q != %q",
            name, pos, ast.PrettyB(blk.Type), ast.PrettyB(ast.Para))
      }

      if !blk.Has(datum.idref) && datum.idref != "" {
         t.Fatalf(
            "%s %d: parser failed to set idref: %s != %q",
            name, pos, blk.Idrefs, datum.idref)
      }
      size = len(blk.Content.Inline)
      if size != datum.size {
         for _, e := range blk.Content.Inline {
            t.Log("Element: ", ast.PrettyE(e.Type), " / ", e.Text)
         }
         t.Fatalf(
            "%s %d: parser returned wrong inlinear parse: %d != %d",
            name, pos, size, datum.size)
      }
   }
}




