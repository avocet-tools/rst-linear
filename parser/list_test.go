package parser

import (
   "strings"
   "testing"
   "gitlab.com/avocet-tools/ast"
)

func Test_lists(t *testing.T){
   name := "parser.Test_lists"
   data := []struct{
      raw []string
      ul ast.BType
      li ast.BType
      key string
      class []string
   }{
      // Test 0
      {
         []string{
            "* Item",
            "* Item",
         }, ast.UList, ast.UListItem, "", []string{},
      },
      // Test 1
      {
         []string{
            "#. Item",
            "#. Item",
         }, ast.OList, ast.OListItem, "", []string{},
      },

      // Test 2
      {
         []string{
            "term : noun : masc.",
            "   Definition",
            "term : noun : fem.",
            "   Definition",
         }, ast.DList, ast.DListItem, "term", []string{"noun", "masc."},
      },
   }

   for pos, datum := range data {
      p := New("list_test", strings.Join(datum.raw, "\n"))
      con := p.Parse()

      size := len(con.Data)
      if size != 1 {
         t.Fatalf(
            "%s %d: parser returned wrong content: %d != 1",
            name, pos, size)
      }
      blk := con.Data[0]
      if blk.Type != datum.ul {
         t.Fatalf(
            "%s %d: parser set wrong list block: %q != %q",
            name, pos, ast.PrettyB(blk.Type), ast.PrettyB(datum.ul))
      }
      size = len(blk.Content.Data)
      if size != 2 {
         t.Fatalf(
            "%s %d: parser returned wrong list count: %d != 2",
            name, pos, size)
      }
      blk = blk.Content.Data[0]
      if blk.Type != datum.li {
         t.Fatalf(
            "%s %d: parser set wrong list block: %q != %q",
            name, pos, ast.PrettyB(blk.Type), ast.PrettyB(datum.li))
      }
      key, ok := blk.Content.Internal["key"]
      if !ok && datum.key != "" {
         t.Fatalf("%s %d: parser failed to set key", name, pos)
      }
      if key != datum.key {
         t.Fatalf(
            "%s %d: parser set wrong key: %q != %q",
            name, pos, key, datum.key)
      }
      if len(datum.class) > 0 {
         for _, cls := range datum.class {
            if !blk.HasClass(cls) {
               t.Fatalf(
                  "%s %d: parser failed to set classifier: %s in %s",
                  name, pos, cls, blk.Class)
            }
         }
      }
   }

}
