package parser

import (
   "strings"
   "testing"
   "github.com/spf13/viper"

   "gitlab.com/avocet-tools/ast"
)

// Test_parse_directives tests directive parse and metadata collection.
func Test_parse_directives(t *testing.T){
   name := "parser.Test_parse_directives"

   // Configure RST/GO note directives for test
   viper.SetDefault("ast.domains.rst.directives.note.type", "admonition")
   viper.SetDefault("ast.domains.rst.directives.note.argument", "block")
   viper.SetDefault("ast.domains.rst.directives.note.content", "parse")
   viper.SetDefault("ast.domains.rst.directives.note.options.class", "")
   viper.SetDefault("ast.domains.go.directives.note.type", "admonition")
   viper.SetDefault("ast.domains.go.directives.note.argument", "block")
   viper.SetDefault("ast.domains.go.directives.note.content", "parse")
   viper.SetDefault("ast.domains.go.directives.note.options.class", "")

   data := []struct{
      raw []string
      dom string
      class string
      size int
   }{
      // Test 0
      {
         []string{".. note::"},
         "rst", "", 0,
      },

      // Test 1
      { 
         []string{".. note:: Content"},
         "rst", "", 1,
      },

      // Test 2
      {
         []string{
            ".. note::",
            "",
            "   Test",
         }, "rst", "", 1,
      },

      // Test 3
      {
         []string{
            ".. note::",
            "   :class: key",
            "",
            "   Test",
         }, "rst", "key", 1,
      },
      // Test 4
      {
         []string{
            ".. note:: arg",
            "   :class: key",
            "",
            "   Test",
         }, "rst", "key", 2,
      },

      // Test 5
      {
         []string{".. go:note::"},
         "go", "", 0,
      },

      // Test 6
      { 
         []string{".. go:note:: Content"},
         "go", "", 1,
      },

      // Test 7
      {
         []string{
            ".. go:note::",
            "",
            "   Test",
         }, "go", "", 1,
      },

      // Test 7
      {
         []string{
            ".. go:note::",
            "   :class: key",
            "",
            "   Test",
         }, "go", "key", 1,
      },

      // Test 8
      {
         []string{
            ".. go:note:: arg",
            "   :class: key",
            "",
            "   Test",
         }, "go", "key", 2,
      },


   }

   for pos, datum := range data {
      p := New("directive_test.go", strings.Join(datum.raw, "\n"))
      con := p.Parse()

      size := len(con.Data)
      blk := con.Data[0]
      if blk.Type != ast.Directive {
         t.Fatalf(
            "%s %d: parser returned wrong content type: %q != %q",
            name, pos, ast.PrettyB(blk.Type), ast.PrettyB(ast.Directive))
      }

      dom, ok := blk.Content.Internal["domain"]
      if !ok {
         t.Fatalf("%s %d: parser failed to set domain", name, pos)
      }
      if dom != datum.dom {
         t.Fatalf(
            "%s %d: parser set wrong domain: %q != %q",
            name, pos, dom, datum.dom)
      }
      dir, ok := blk.Content.Internal["directive_type"]
      if !ok {
         t.Fatalf("%s %d: parser failed to set directive_type", name, pos)
      }

      if dir != "note" {
         t.Fatalf("%s %d: parser set wrong directive_type: %s != note", name, pos, dir)
      }
      if datum.class != "" {
         cls, ok := blk.Content.Meta["class"]
         if !ok {
            t.Fatalf("%s %d: parser failed to set class", name, pos)
         }
         if cls != datum.class {
            t.Fatalf(
               "%s %d: parser set wrong class: %q !=%q",
               name, pos, cls, datum.class)
         }
      }

      size = len(blk.Content.Data)
      if size != datum.size {
         for _, b := range blk.Content.Data {
            t.Log("Type: ", ast.PrettyB(b.Type))
            text := []string{}
            for _, e := range b.Content.Inline {
               text = append(text, e.Text)
            }
            t.Log("Text:", text)
         }
         t.Fatalf(
            "%s %d: parser subparsed wrong content: %d != %d",
            name, pos, size, datum.size)
      }
   }
}
