package lexer

import (
   "strings"
   "testing"
   "gitlab.com/avocet-tools/linear/token"
)

func Test_head(t *testing.T){
   name := "lexer.Text_head"
   chars := []string{
      "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".",
      "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`",
      "{", "|", "}", "~",
   }

   for pos, char := range chars {
      l := New("head_test.go", strings.Repeat(char, 5))
      tok := l.Next()

      if tok.Type != token.Headline {
         t.Fatalf(
            "%s %d: lexer returned wrong token on %q: %q != %q",
            name, pos, char, token.Pretty(tok.Type), token.Pretty(token.Headline))
      }
   }

}
