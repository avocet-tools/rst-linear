package linear
 
import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

   "github.com/spf13/viper"

	"gitlab.com/avocet-tools/ast"
	"gitlab.com/avocet-tools/linear/parser"
)

func split(key string) (string, string){
   t := viper.GetString(key)
   if t != "" {
      s := strings.Split(t, ":")
      size := len(s)
      if size > 0 {
         if size == 1 {
            return "rst", s[0]
         }
         return s[0], s[1]
      }
   }
   return "", ""
}

func set(dom, dir, key, val string){
   fmt.Printf("Set %s:%s %s parse to %s", dom, dir, key, val)
   viper.Set(
      fmt.Sprintf(
         "ast.domains.%s.directives.%s.%s",
         dom, dir, key),
      val)
}

func directiveConf(){
   dom, dir := split("parse-directive")
   set(dom, dir, "content", "parse")
   dom, dir = split("block-directive")
   set(dom, dir, "argument", "block")
}

func RunParse(args []string){
   initLogger()
   info("Called parse operation")
   directiveConf()
   start := time.Now()
   con := Parse("stdin", strings.Join(args, "\n"))

   debug("Marshaling Content to JSON")
   data, err := json.MarshalIndent(con, " ", " ")
   if err != nil {
      fatal(err)
   }
   fmt.Println(string(data))
   fmt.Println("Parse Complete:", time.Since(start))
}

func Parse(fname, raw string) *ast.Content {
   p := parser.New(fname, raw)
   return p.Parse()
}

