package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"testing"
)

func Test_meta(t *testing.T) {
	name := "lexer.Test_meta"
	data := []struct {
		raw   string
		key   string
		text  string
		ttype token.Type
	}{
		{":key:", "key", "", token.Meta},
		{":key: val", "key", "val", token.Meta},
		{"   :key:", "key", "", token.IndentMeta},
		{"   :key: val", "key", "val", token.IndentMeta},
	}

	for pos, datum := range data {
		l := New("meta_test.go", datum.raw)
		tok := l.Next()

		if tok.Type != datum.ttype {
			t.Fatalf(
				"%s %d: lexer set wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(datum.ttype))
		}

		if tok.Key != datum.key {
			t.Fatalf(
				"%s %d: lexer set wrong key: %q != %q",
				name, pos, tok.Key, datum.key)
		}

		if tok.Text != datum.text {
			t.Fatalf(
				"%s %d: lexer set wrong text: %q != %q",
				name, pos, tok.Text, datum.text)
		}
	}
}
