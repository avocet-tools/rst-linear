package parser

import (
   "fmt"
   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/linear/token"
)

func (p *Parser) analyze(){
   switch p.prime.Type {
   case token.Empty:
   case token.Text:
      p.parseText()
   case token.Slug:
      p.parseSlug()
   case token.UListItem:
      p.parseUList()
   case token.OListItem:
      p.parseOList()
   case token.DListItem:
      p.parseDList()
   case token.Directive:
      p.parseDirective()
   case token.Meta, token.IndentMeta:
      p.setMeta()
   case token.Target:
      p.parseTarget()
   case token.Headline:
      p.parseSection()
   case token.Footnote:
      p.parseNote(ast.Footnote)
   case token.Endnote:
      p.parseNote(ast.Endnote)
   default:
      fmt.Printf("Unknown Token: %s(%d)\n", token.Pretty(p.prime.Type), p.prime.Size)
   }
}
