package parser

import (
   "strings"
   "testing"
)

func Test_targets(t *testing.T){
   name := "parser.Test_targets"

   raw := []string{
      ".. _test-0: value",
      ".. _test-1:\n   value",
      ".. _`test 2`: value",
      ".. _`test 3`:\n   value",
   }

   p := New("target_test.go", strings.Join(raw, "\n\n"))
   con := p.Parse()

   size := len(con.Targets)
   if size != 4 {
      t.Fatalf(
         "%s: parser missed target(s): %d != %d",
         name, size, 4)
   }
   for _, key := range []string{"test-0", "test-1", "test 2", "test 3"}{
      targ, ok := con.Targets[key]
      if !ok {
         t.Fatalf("%s: parser failed to set target %q", name, key)
      }
      size = len(targ.Data)
      if size != 1 {
         t.Fatalf(
            "%s parser set wrong content for %q: %d != %d",
            name, key, size, 1)
      }
      size = len(targ.Data[0].Content.Inline)
      if size != 1 {
         t.Fatalf(
            "%s: parser subpared wrong inline content on %q: %d != %d",
            name, key, size, 1)
      }
      e := targ.Data[0].Content.Inline[0]
      if e.Text != "value" {
         t.Fatalf(
            "%s: parser set wrong value on %q: %q != %q",
            name, key, e.Text, "value")
      }

   }
}

