package token

import (
	"regexp"
	"testing"
)

func compile(re string) *regexp.Regexp {
	return regexp.MustCompile(re)
}

func Test_clear(t *testing.T) {
	name := "linear/token.Test_clear"
	raw := "* Item"
	text := "Item"
	re := regexp.MustCompile("^\\* +")

	prime := '*'

	tok := New("token_test.go", raw, 1)

	if tok.Raw != raw {
		t.Fatalf(
			"%s: token failed to set raw text: %q != %q",
			name, tok.Raw, raw)
	}
	if tok.Prime != prime {
		t.Fatalf(
			"%s: token failed to set prime rune: %s != %q",
			name, string(tok.Prime), string(prime))
	}
	tok.Clear(re)
	if tok.Text != text {
		t.Fatalf(
			"%s token failed to set text after clear: %q != %q",
			name, tok.Text, text)
	}

}
