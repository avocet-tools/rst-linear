package token

import "fmt"

// Type is an integer indicating the classification of a token.
type Type int

// Specifies Token Type values
const (
	Unset Type = iota
	EOF
	Empty
	Text
	Indent
	IndentMeta
	UList
	UListItem
	OList
	OListItem
	Directive
	Target
	Slug
	Meta
	Footnote
	Endnote
	Citation
	Headline
   Title
	Comment
	Block
   DListItem
)

// Pretty takes a token type and returns a human readable string.
func Pretty(t Type) string {
	switch t {
   case Title:
      return "title-token"
	case Block:
		return "block-token"
	case Unset:
		return "unset-token"
	case EOF:
		return "end-of-file-token"
	case Empty:
		return "empty-token"
	case Text:
		return "text-token"
	case Indent:
		return "indent-token"
	case IndentMeta:
		return "indent-metadata-token"
	case UList:
		return "ulist-token"
	case UListItem:
		return "ulistitem-token"
	case OList:
		return "olist-token"
	case OListItem:
		return "olistitem-token"
	case Directive:
		return "directive-token"
	case Target:
		return "target-token"
	case Slug:
		return "identity-reference-token"
	case Meta:
		return "metadata-token"
	case Footnote:
		return "footnote-token"
	case Endnote:
		return "endnote-token"
	case Citation:
		return "citation-token"
	case Headline:
		return "headline-token"
	case Comment:
		return "comment-token"
	default:
		return fmt.Sprintf("unknown-token(%d)", t)
	}
}
