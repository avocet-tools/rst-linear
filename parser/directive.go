package parser

import (

   "fmt"
   "github.com/spf13/viper"

   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/linear/token"
)

func (p *Parser) inblock(tok *token.Token, toks []*token.Token) []*token.Token{
   return append([]*token.Token{p.prime}, toks...)
}

func (p *Parser) parseDirective(){

   blk := ast.NewBlock(ast.Directive, p.file, p.prime.Line)
   dom := p.prime.Dom
   dir := p.prime.Dir

   con := viper.GetString(fmt.Sprintf("ast.domains.%s.directives.%s.content", dom, dir))
   arg := viper.GetString(fmt.Sprintf("ast.domains.%s.directives.%s.argument", dom, dir))

   toks := p.prime.Sub
   switch arg {
   case "block":
      toks = p.inblock(p.prime, toks)
      toks[0].Sub = []*token.Token{}
   case "none", "string":
      blk.Content.Internal["argument"] = arg
   case "title":
      if p.prime.Text != "" {
         blk.Content.Inline = p.parseInline(p.prime.Text, p.prime.Line)
      }
   }

   switch con {
   case "parse":
      blk.Content = p.subparse(toks)
   default:
      blk.Content.Merge(p.preserve(toks))
   }
   blk.Content.Internal["domain"] = dom
   blk.Content.Internal["directive_type"] = dir
 
   p.add(blk)
}
