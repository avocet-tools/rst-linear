


all: test lint fmt build

fmt:
	gofmt -w .

dep:
	go get -v -d ./...

test:
	go test -v ./...

test-short: dep
	go test -short ./...

lint:
	golint ./...

build:
	go build ./cmd/linear


