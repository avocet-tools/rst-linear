package parser

import (
   "gitlab.com/avocet-tools/linear/token"
)

func (p *Parser) parseTarget(){
   key := p.prime.Key
   toks := append([]*token.Token{p.prime}, p.prime.Sub...)
   con := p.subparse(toks)

   p.con.Targets[key] = con
}
