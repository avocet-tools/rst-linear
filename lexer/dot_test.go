package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"testing"
)

func Test_targets(t *testing.T) {
	name := "lexer.Test_targets"
	data := []struct {
		raw  string
		key  string
		text string
	}{
		{".. _key:", "key", ""},
		{".. _key: example", "key", "example"},
		{".. _`tick key`:", "tick key", ""},
		{".. _`tick key`: example", "tick key", "example"},
	}

	for pos, datum := range data {
		l := New("dot_test.go", datum.raw)
		tok := l.Next()

		if tok.Type != token.Target {
			t.Fatalf(
				"%s %d: lexer set wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(token.Target))
		}

		if tok.Key != datum.key {
			t.Fatalf(
				"%s %d: lexer set wrong key: %q != %q",
				name, pos, tok.Key, datum.key)
		}

		if tok.Text != datum.text {
			t.Fatalf(
				"%s %d: lexer set wrong text: %q != %q",
				name, pos, tok.Text, datum.text)
		}
	}

}

func Test_directives(t *testing.T) {
	name := "lexer.Test_directive"
	data := []struct {
		raw  string
		key  string
		dom  string
		dir  string
		text string
	}{
		{".. config::", "", "", "config", ""},
		{".. config:: key", "key", "", "config", "key"},
		{".. go:config::", "", "go", "config", ""},
		{".. go:config:: key", "key", "go", "config", "key"},
		{".. |key| config::", "key", "", "config", ""},
		{".. |key| config:: text", "key", "", "config", "text"},
		{".. |key| go:config::", "key", "go", "config", ""},
		{".. |key| go:config:: text", "key", "go", "config", "text"},
	}
	for pos, datum := range data {
		l := New("dot_test.go", datum.raw)
		tok := l.Next()

		if tok.Type != token.Directive {
			t.Fatalf(
				"%s %d: lexer set wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(token.Target))
		}

		if tok.Key != datum.key {
			t.Fatalf(
				"%s %d: lexer set wrong key: %q != %q",
				name, pos, tok.Key, datum.key)
		}

		if tok.Text != datum.text {
			t.Fatalf(
				"%s %d: lexer set wrong text: %q != %q",
				name, pos, tok.Text, datum.text)
		}

		if tok.Dom != datum.dom {
			t.Fatalf(
				"%s %d: lexer set wrong domain: %q != %q",
				name, pos, tok.Dom, datum.dom)
		}

		if tok.Dir != datum.dir {
			t.Fatalf(
				"%s %d: lexer set wrong directive: %q != %q",
				name, pos, tok.Dir, datum.dir)
		}
	}
}

func Test_notes(t *testing.T) {
	name := "lexer.Test_notes"
	data := []struct {
		raw   string
		key   string
		text  string
		ttype token.Type
	}{
		{".. [note] Text", "note", "Text", token.Footnote},
		{".. [[note]] Text", "note", "Text", token.Endnote},
	}

	for pos, datum := range data {
		l := New("dot_test.go", datum.raw)
		tok := l.Next()

		if tok.Type != datum.ttype {
			t.Fatalf(
				"%s %d: lexer set wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(token.Target))
		}

		if tok.Key != datum.key {
			t.Fatalf(
				"%s %d: lexer set wrong key: %q != %q",
				name, pos, tok.Key, datum.key)
		}

		if tok.Text != datum.text {
			t.Fatalf(
				"%s %d: lexer set wrong text: %q != %q",
				name, pos, tok.Text, datum.text)
		}
	}

}
