package preparser

import (
   "fmt"
   "strings"
   "regexp"
   "gitlab.com/avocet-tools/linear/token"
)

var (
   bindent = regexp.MustCompile(fmt.Sprintf("^%s", strings.Repeat(" ", 2)))
   trindent = regexp.MustCompile(fmt.Sprintf("^%s", strings.Repeat(" ", 3)))
   hexindent = regexp.MustCompile(fmt.Sprintf("^%s", strings.Repeat(" ", 6)))
)

func (p *Preparser) analyze(tok *token.Token){
   switch tok.Type {
   case token.Text:
      p.preparseText(tok)
   case token.DListItem:
      p.preparseIndent(tok, p.isTrindent, trindent)
   case token.UListItem:
      p.preparseIndent(tok, p.isBindent, bindent)
   case token.OListItem:
      p.preparseIndent(tok, p.isTrindent, trindent)
   case token.Footnote:
      p.preparseIndent(tok, p.isTrindent, trindent)
   case token.Endnote:
      p.preparseIndent(tok, p.isTrindent, trindent)
   case token.Comment:
      p.preparseIndent(tok, p.isTrindent, trindent)
   case token.Directive:
      // Set Default Domain
      if tok.Dom == "" {
         tok.Dom = p.dom
      }

      // Reset Default Domain
      if tok.Dir == "default-domain" {
         p.dom = strings.TrimSpace(tok.Text)
      }
      p.preparseIndent(tok, p.isTrindent, trindent)
   case token.Target:
      p.preparseTarget(tok)
   case token.Block:
      p.preparseBlock(tok)
   }
}

func (p *Preparser) preparseBlock(tok *token.Token){
   for p.secunde.Type == token.Block {
      tok.Sub = append(tok.Sub, p.secunde)
      p.read()
   }
}

func (p *Preparser) isBindent(tok *token.Token) bool {
   return tok.Prime == ' ' && tok.Secunde == ' '
}

func (p *Preparser) isTrindent(tok *token.Token) bool {
   return (
      tok.Prime == ' ' && 
      tok.Secunde == ' ' && 
      tok.Terte == ' ') || tok.Type == token.IndentMeta
}

func (p *Preparser) isHexindent(tok *token.Token) bool {
   return hexindent.MatchString(tok.Text)
}

func (p *Preparser) isEmpty(tok *token.Token) bool {
   return tok.Type == token.Empty 
}

func (p *Preparser) primeEOF() bool {
   return p.prime.Type == token.EOF
}

func (p *Preparser) preparseTarget(tok *token.Token){
   if p.isTrindent(p.secunde) {
      p.preparseIndent(tok, p.isTrindent, trindent)
      if tok.Prime == '|' {
         tok.Set("")
      }
      return
   } else if tok.Size == 0 {
      tok.Type = token.Slug
   }
}


func (p *Preparser) preparseText(tok *token.Token){
   for p.secunde.Type == token.Text {
      tok.Sub = append(tok.Sub, p.secunde)
      p.read()
   }
}

func (p *Preparser) preparseIndent(
   tok *token.Token, 
   indent func(*token.Token) bool,
   clear *regexp.Regexp){

      if indent(p.secunde) || p.isEmpty(p.secunde){
         for (indent(p.secunde) || p.isEmpty(p.secunde)) && !p.primeEOF() {
            p.secunde.Clear(clear)
            tok.Append(p.secunde)
            if p.secunde.Type == token.IndentMeta || p.secunde.Type == token.Meta{
               p.secunde.Type = token.Meta
               stok := p.secunde
               text := []string{}
               p.read()
               for p.isHexindent(p.secunde){
                  p.secunde.Clear(hexindent)
                  text = append(text, p.secunde.Text)
                  p.read()
               }
               stok.Text = strings.Join(append([]string{stok.Text}, text...), "\n")
            } else {
               p.read()
            }
         }
      }
}

   


