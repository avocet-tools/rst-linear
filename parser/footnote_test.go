package parser

import (
   "strings"
   "testing"
   "gitlab.com/avocet-tools/ast"
)

func Test_notes(t *testing.T){
   name := "parser.Test_notes"
   data := []struct{
      raw []string
      key string
      size int
      t ast.BType
   }{
      // Test 0
      {
         []string{
            ".. [key] Text",
         }, "key", 1, ast.Footnote,
      },
      // Test 1
      {
         []string{
            ".. [key] Text",
            "",
            "   Test",
         }, "key", 2, ast.Footnote,
      },
      // Test 2
      {
         []string{
            ".. [[key]] Text",
         }, "key", 1, ast.Endnote,
      },
      // Test 3
      {
         []string{
            ".. [[key]] Text",
            "",
            "   Test",
         }, "key", 2, ast.Endnote,
      },
   }

   for pos, datum := range data {
      p := New("footnote_test.go", strings.Join(datum.raw, "\n"))
      con := p.Parse()
      size := len(con.Data)

      if size != 1 {
         t.Fatalf(
            "%s %d: parser returned wrong content: %d != %d",
            name, pos, size, 1)
      }
      blk := con.Data[0]
      if blk.Type != datum.t {
         t.Fatalf(
            "%s %d: parser returned wrong block type: %q != %q",
            name, pos, ast.PrettyB(blk.Type), ast.PrettyB(datum.t))
      }

      key, ok := blk.Content.Internal["key"]
      if !ok {
         t.Fatalf("%s %d: parser failed to set key", name, pos)
      }
      if key != datum.key {
         t.Fatalf(
            "%s %d: parser set wrong key: %q != %q",
            name, pos, key, datum.key)
      }
      size = len(blk.Content.Data)
      if size != datum.size {
         for _, b := range blk.Content.Data {
            text := []string{}
            for _, e := range b.Content.Inline {
               text = append(text, e.Text)
            }
            t.Log("Type: ", text)
         }
         t.Fatalf(
            "%s %d: subparser returned wrong content: %d != %d",
            name, pos, size, datum.size)
      }

   }

}
