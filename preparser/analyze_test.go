package preparser

import (
   "strings"
   "testing"
   "gitlab.com/avocet-tools/linear/token"
)

func Test_analyze(t *testing.T){
   name := "preparser.Test_analyze"
   data := []struct{
      raw []string
      ttype token.Type
      size int
   }{
      // Test 0
      {
         []string{
            "Text",
            "text",
            "text",
         }, token.Text, 2,
      },

      // Test 1
      {
         []string{
            "* Item",
            "  sub",
            "  sub",
            "",
            "  sub",
         }, token.UListItem, 4,
      },

      // Test 2
      {
         []string{
            "#. Item",
            "   sub",
         }, token.OListItem, 1,
      },

      // Test 3
      {
         []string{
            ".. directive:: key",
            "   :metadata: value",
            "",
            "   Content",
         }, token.Directive, 3,
      },

      // Test 4
      {
         []string{
            ":metadata: value",
            "   second",
         }, token.Meta, 0,
      },

      // Test 5
      {
         []string{
            ".. _key:",
            "   second",
         }, token.Target, 1,
      },

      // Test 6
      {
         []string{
            ".. _key:",
         }, token.Slug, 0,
      },

      // Test 7
      {
         []string{
            "term",
            "   Definition",
         }, token.DListItem, 1,
      },

      // Test 8
      {
         []string{
            "term",
            "   Definition",
            "",
            "   Definition",
         }, token.DListItem, 3,
      },
   }

   for pos, datum := range data {
      p := New("Test_analyze", strings.Join(datum.raw, "\n"))
      tok := p.Next()

      if tok.Type != datum.ttype {
         t.Fatalf(
            "%s %d: preparser returned wrong type: %q != %q",
            name, pos, token.Pretty(tok.Type), token.Pretty(datum.ttype))
      }
      size := len(tok.Sub)
      if size != datum.size {
         t.Fatalf(
            "%s %d: preparser added wrong subtokens: %d != %d",
            name, pos, size, datum.size)
      }
   }
}
