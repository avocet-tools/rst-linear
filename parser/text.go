package parser

import (
   "strings"
   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/inlinear"
   "gitlab.com/avocet-tools/linear/token"
)

func (p *Parser) parseText(){
   blk := ast.NewBlock(ast.Para, p.file, p.prime.Line)

   // Parse Inlinear Content
   text := []string{p.prime.Text}
   for _, tok := range p.prime.Sub {
      if tok.Type == token.Meta || tok.Type == token.IndentMeta {
         blk.Content.Meta[tok.Key] = tok.Text
         continue
      }
      text = append(text, tok.Text)
   }
   blk.Content.Inline = inlinear.Parse(p.file, strings.Join(text, "\n"), p.prime.Line)

   p.add(blk)

}
