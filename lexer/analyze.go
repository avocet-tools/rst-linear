package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
)

func (l *Lexer) analyze(tok *token.Token) {
	if l.eof {
		tok.Type = token.EOF
		return
	}

	switch tok.Prime {
	case 0:
		tok.Type = token.Empty
	case ' ':
		l.lexSpace(tok)
	case '*', '+', '-':
		l.lexUList(tok)
	case '.':
		l.lexDots(tok)
	case ':':
		l.lexColon(tok)
	case '|':
		l.lexBlock(tok)
	default:
      switch tok.Type {
      case token.Meta, token.IndentMeta:
         tok.Type = token.Meta
      default:
         l.lexText(tok)
      }
	}
}
