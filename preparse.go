package linear

import (
   "encoding/json"
   "fmt"
   "strings"
   "time"

   "gitlab.com/avocet-tools/linear/preparser"
   "gitlab.com/avocet-tools/linear/token"
)

func RunPreparse(args []string){
   initLogger()
   info("Called preparse operation")
   start := time.Now()
   toks := Preparse("stdin", strings.Join(args, "\n"))

   debug("Marshaling Tokens to JSON")
   data, err := json.MarshalIndent(toks, " ", " ")
   if err != nil {
      fatal(err)
   }
   trace("Marshal Complete")

   fmt.Println(string(data))
   fmt.Println("Preparse Complete:", len(toks), "in", time.Since(start))
}

func Preparse(fname, raw string) []*token.Token {
   p := preparser.New(fname, raw)
   toks := []*token.Token{}
   tok := p.Next()

   for tok.Type != token.EOF {
      toks = append(toks, tok)
      tok = p.Next()
   }

   return toks
}
