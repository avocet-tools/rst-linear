package main

import (
   "github.com/spf13/cobra"
   "github.com/spf13/viper"
   "gitlab.com/avocet-tools/linear"
)

// Version specifies the current version of the library.
const Version = "0.1.0"

var (
   cmd = cobra.Command{
      Use: "linear",
      Short: "ReStructuredText Linear Lexer and Parser",
      Version: Version,
   }

   lex = cobra.Command{
      Use: "lex",
      Short: "Lexes the given string",
      Run: func(c *cobra.Command, args []string){
         linear.RunLex(args)
      },
   }

   preparse = cobra.Command{
      Use: "preparse",
      Short: "Lexes and preparses the given string",
      Run: func(c *cobra.Command, args[]string){
         linear.RunPreparse(args)
      },
   }
   parse = cobra.Command{
      Use: "parse",
      Short: "Parses the given string",
      Run: func(c *cobra.Command, args[]string){
         linear.RunParse(args)
      },
   }
)

func init(){

	// Configure Debugging Defaults
	viper.SetDefault("log.debug.inlinear.prog", false)
	viper.SetDefault("log.debug.inlinear.lexer", false)
	viper.SetDefault("log.debug.inlinear.preparser", false)
	viper.SetDefault("log.debug.inlinear.parser", false)
	viper.SetDefault("verbosity", false)

	cmd.PersistentFlags().BoolP(
		"debug", "D",
		viper.GetBool("log.debug.rst-inlinear.prog"),
		"debug logs for main process")
	viper.BindPFlag("log.debug.rst-inlinear.prog", cmd.PersistentFlags().Lookup("debug"))

	cmd.PersistentFlags().BoolP(
		"debug-lexer", "",
		viper.GetBool("log.debug.rst-inlinear.lexer"),
		"debug logs for the lexer")
	viper.BindPFlag("log.debug.rstinlinear.lexer", cmd.PersistentFlags().Lookup("debug-lexer"))

	cmd.PersistentFlags().BoolP(
		"debug-preparser", "",
		viper.GetBool("log.debug.rst-inlinear.preparser"),
		"debug logs for the preparser")
	viper.BindPFlag("log.debug.rstinlinear.preparser", cmd.PersistentFlags().Lookup("debug-preparser"))

	cmd.PersistentFlags().BoolP(
		"debug-parser", "",
		viper.GetBool("log.debug.rst-inlinear.parser"),
		"debug logs for the parser")
	viper.BindPFlag("log.debug.rstinlinear.parser", cmd.PersistentFlags().Lookup("debug-parser"))

	cmd.PersistentFlags().BoolP(
		"verbose", "v",
		viper.GetBool("verbosity"),
		"logs verbose")
	viper.BindPFlag("verbosity", cmd.PersistentFlags().Lookup("verbose"))

   parse.PersistentFlags().StringP(
      "parse-directive", "",
      "",
      "specifies directives for parse command")
   viper.BindPFlag("parse-directive", parse.PersistentFlags().Lookup("parse-directive"))

   parse.PersistentFlags().StringP(
      "block-directive", "",
      "",
      "specifies directives for block parse")
   viper.BindPFlag("block-directive", parse.PersistentFlags().Lookup("block-directive"))
}

func main() {
   cmd.AddCommand(&lex)
   cmd.AddCommand(&preparse)
   cmd.AddCommand(&parse)
   cmd.Execute()
}
