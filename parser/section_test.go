package parser

import (
   "strings"
   "testing"
)

func Test_section(t *testing.T){
   name := "parser.Test_section"
   data := []struct{
      raw []string
      lit string
      conSize int
      secSize int
   }{
      // Test 0
      {
         []string{
            "",
            "###",
            "",
         }, "#", 1, 0,
      },
      // Test 1
      {
         []string{
            "",
            "Title",
            "#####",
            "",
         }, "#", 1, 1,
      },
      // Test 2
      {
         []string{
            "",
            "#####",
            "Title",
            "#####",
            "",
         }, "#", 1, 1,
      },
      // Test 3
      {
         []string{
            "",
            "###",
            "",
            "###",
         }, "#", 2, 0,
      },
      // Test 4
      {
         []string{
            "",
            "###",
            "",
            "===",
         }, "#", 1, 0,
      },
   }

   for pos, datum := range data {
      p := New("section_test.go", strings.Join(datum.raw, "\n"))
      con := p.Parse()

      size := len(con.Sections)
      if size != datum.conSize {
         for _, sec := range con.Sections{
            t.Log("Section: ", sec.LiteralLevel)
         }
         t.Fatalf(
            "%s %d: parser set wrong sections: %d != %d",
            name, pos,size, datum.conSize)
      }
      sec := con.Sections[0]

      if sec.LiteralLevel != datum.lit {
         t.Fatalf(
            "%s %d: parser set wrong literal level: %q != %q",
            name, pos, sec.LiteralLevel, datum.lit)
      }
      size = len(sec.Content.Inline)
      if size != datum.secSize {
         t.Fatalf(
            "%s %d: parser set wrong section content: %d != %d",
            name, pos, size, datum.secSize)
      }
   }

}
