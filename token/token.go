package token

import "regexp"

// Token provides a struct for storing data during lexical analysis and preparser operations.
type Token struct {
	Type Type `json:"token_type"`

	Raw   string `json:"raw"`
	Text  string `json:"text"`
	Runes []rune `json:"runes"`
	Size  int    `json:"size"`

	Prime   rune `json:"rune_prime"`
	Secunde rune `json:"rune_seconde"`
	Terte   rune `json:"rune_terte"`
	Quarte  rune `json:"rune_quarte"`
	Quinte  rune `json:"rune_quinte"`

	File string `json:"filename"`
	Line int    `json:"line"`

	Key string `json:"key,omitempty"`
	Dom string `json:"domain,omitempty"`
	Dir string `json:"directive_type,omitempty"`

   Sub []*Token `json:"subordinate_tokens,omitempty"`
}

// New takes a filename, raw string, and line number then returns a Token pointer.
func New(fname, raw string, line int) *Token {
	tok := &Token{
		Raw:  raw,
		File: fname,
		Line: line,
      Sub: []*Token{},
	}
	tok.Set(raw)

	return tok
}

func (tok *Token) get(i int) rune {
	if i >= tok.Size {
		return 0
	}
	return tok.Runes[i]
}

// Set method  takes a text string and then reinitializes the Text, Runes, Size and runic
// positional attributes.
func (tok *Token) Set(text string) {
	tok.Text = text
	tok.Runes = []rune(text)
	tok.Size = len(tok.Runes)

	tok.Prime = tok.get(0)
	tok.Secunde = tok.get(1)
	tok.Terte = tok.get(2)
	tok.Quarte = tok.get(3)
	tok.Quinte = tok.get(4)
}

// Clear method takes a precompiled Regular Expression pointer and uses it to clear content
// from the Text attribute and reset the Size and runic attributes.
func (tok *Token) Clear(re *regexp.Regexp) {
	tok.Set(re.ReplaceAllString(tok.Text, ""))
}

// Append adds a token to the Sub array
func (tok *Token) Append(t *Token){
   tok.Sub = append(tok.Sub, t)
}
