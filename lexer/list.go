package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"regexp"
	"unicode"
)

var (
	ulist     = regexp.MustCompile("^[-+*] +")
	listKey   = regexp.MustCompile("^\\[.*?\\] *")
	exListKey = regexp.MustCompile("^\\[(.*?)\\] *")
	olist     = regexp.MustCompile("^[0-9A-Za-z#]+\\. +?")
)

func (l *Lexer) lexUList(tok *token.Token) {
	if tok.Secunde == ' ' {
		tok.Type = token.UListItem
		tok.Clear(ulist)
		l.extractListKey(tok)
		return
	} else if l.isHeadline(tok) {
		tok.Type = token.Headline
		return
	}
	tok.Type = token.Text

}

func (l *Lexer) lexText(tok *token.Token) {
	if tok.Secunde == '.' && tok.Terte == ' '{
		l.lexOList(tok)
		return
	} else if unicode.IsNumber(tok.Prime) {
		if olist.MatchString(tok.Text) {
			l.lexOList(tok)
			return
		}
	} else if l.isHeadline(tok) {
		tok.Type = token.Headline
		return
	}
	tok.Type = token.Text
}

func (l *Lexer) lexOList(tok *token.Token) {
	tok.Type = token.OListItem
	tok.Clear(olist)
	l.extractListKey(tok)
}

func (l *Lexer) extractListKey(tok *token.Token) {
	res := exListKey.FindAllStringSubmatch(tok.Text, -1)
	if len(res) > 0 {
		if len(res[0]) > 1 {
			tok.Key = res[0][1]
			tok.Clear(listKey)
		}
	}
}
