package lexer

import (
	"gitlab.com/avocet-tools/linear/token"
	"testing"
)

func Test_lists(t *testing.T) {
	name := "lexer.Test_lists"
	data := []struct {
		raw   string
		ttype token.Type
		text  string
		key   string
	}{
		{"* Item", token.UListItem, "Item", ""},
		{"* [key] Item", token.UListItem, "Item", "key"},
		{"+ Item", token.UListItem, "Item", ""},
		{"+ [key] Item", token.UListItem, "Item", "key"},
		{"- Item", token.UListItem, "Item", ""},
		{"#. [key] Item", token.OListItem, "Item", "key"},
		{"1. Item", token.OListItem, "Item", ""},
		{"10. Item", token.OListItem, "Item", ""},
		{"10000. Item", token.OListItem, "Item", ""},
		{"A. Item", token.OListItem, "Item", ""},
		{"a. Item", token.OListItem, "Item", ""},
	}

	for pos, datum := range data {
		l := New("list_test.go", datum.raw)
		tok := l.Next()

		if tok.Type != datum.ttype {
			t.Fatalf(
				"%s %d: lexer set wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(datum.ttype))
		}
		if tok.Text != datum.text {
			t.Fatalf(
				"%s %d: lexer set wrong text: %q != %q",
				name, pos, tok.Text, datum.text)
		}
		if tok.Key != datum.key {
			t.Fatalf(
				"%s %d: lexer set wrong key: %q != %q",
				name, pos, tok.Key, datum.key)
		}
	}

}
