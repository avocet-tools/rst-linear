package parser

import (
   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/linear/token"
)

func (p *Parser) parseNote(t ast.BType){
   blk := ast.NewBlock(t, p.prime.File, p.prime.Line)

   blk.Content.Internal["key"] = p.prime.Key

   toks := append([]*token.Token{p.prime}, p.prime.Sub...)
   p.prime.Sub = []*token.Token{}

   blk.Content.Merge(p.subparse(toks))

   p.add(blk)
}
