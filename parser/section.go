package parser

import (
   "gitlab.com/avocet-tools/ast"
)

func (p *Parser) parseSection(){
   char := string(p.prime.Prime)
   sec := ast.NewSection(char)

   if len(p.prime.Sub) > 0 {
      sec.Content.Inline = p.parseInline(p.prime.Sub[0].Text, p.prime.Line)
   }

   p.sec(sec)
}
